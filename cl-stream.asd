;;;; cl-stream.asd

(asdf:defsystem #:cl-stream
  :description "Common Lisp client for Stream.IO"
  :author "Mariano Montone <marianomontone@gmail.com>"
  :license "MIT"
  :serial t
  :components ((:file "package")
               (:file "cl-stream"))
  :depends-on (:drakma
			   :cl-json
			   :quri
			   :ironclad
			   :trivial-utf-8))
