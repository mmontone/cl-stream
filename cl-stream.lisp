;;;; cl-stream.lisp

(in-package #:cl-stream)

;;; "cl-stream" goes here. Hacks and glory await!

(defparameter *api-base-url* (quri:uri "https://api.getstream.io/api/v1.0/"))
(defparameter *app-name* "mmontone")
(defparameter *app-id* "5704")
(defparameter *app-location* "eu-central")
(defparameter *api-key* "wu89b7ymbtgr")
(defparameter *api-secret* "n8qxwbxybkc94x4ewqdh4ecm7w5gmbf423h5dmr44gqztk5ub4zuevvsar8f6dkd")

(defun sign-feed (api-secret feed-id)
  (let* ((hashed-secret (let ((digester (ironclad:make-digest 'ironclad:sha1)))
                          (ironclad:update-digest digester
                                                  (trivial-utf-8:string-to-utf-8-bytes api-secret))
                          (ironclad:produce-digest digester)))
         (hmac (let ((hmac (ironclad:make-hmac hashed-secret 'ironclad:sha1)))
                 (ironclad:update-hmac hmac (trivial-utf-8:string-to-utf-8-bytes feed-id))
                 hmac)))
                                        ;(base64:usb8-array-to-base64-string (ironclad:hmac-digest hmac) :uri t)
    (safe-url (base64:usb8-array-to-base64-string (ironclad:hmac-digest hmac)))
    ))

(defun safe-url (url)
  (let ((safe-url url))
    (setf safe-url (ppcre:regex-replace-all "\\+" safe-url "-"))
    (setf safe-url (ppcre:regex-replace-all "\\/" safe-url "_"))
    (setf safe-url (ppcre:regex-replace-all "^=+/" safe-url ""))
    (setf safe-url (ppcre:regex-replace-all "=+$" safe-url ""))
    safe-url))

(defun api-url (url)
  (let ((new-url (quri:merge-uris (quri:uri url) *api-base-url*)))
    (push (cons "api_key" *api-key*)
          (quri:uri-query-params new-url))
    new-url))

(defun feed-id (feed-slug user)
  (format nil "~A~A" feed-slug user))

(defun api-get (uri feed-slug user-id &key (api-secret *api-secret*))
  (let ((api-url (api-url (quri:uri uri))))
    (json:decode-json-from-string
     (babel:octets-to-string
      (drakma:http-request
       (quri:render-uri api-url)
       :additional-headers
       `(("Authorization" . ,(format nil "~A ~A"
                                     (feed-id feed-slug user-id)
                                     (sign-feed api-secret (feed-id feed-slug user-id)))))
       :accept "application/json")))))

(defun api-post (uri feed-slug user-id content &key (api-secret *api-secret*) (async nil))
  (json:decode-json-from-string
   (babel:octets-to-string
    (drakma:http-request (quri:render-uri (api-url (quri:uri uri)))
                         :method :post
                         :content (if (listp content)
                                      (json:encode-json-alist-to-string content)
                                      content)
                         :additional-headers
                         `(("Authorization" . ,(format nil "~A ~A"
                                                       (feed-id feed-slug user-id)
                                                       (sign-feed api-secret (feed-id feed-slug user-id)))))
                         :content-type "application/json"
                         ))))

#+nil(defclass feed ()
       ((id :initarg :id
            :accessor feed-id
            :initform (error "Provide the id"))
        (user :initarg :user
              :accessor feed-user
              :initform (error "Provide the user"))))


(defun get-activities (feed-slug user &key limit offset)
  (let ((uri (quri:uri (format nil "feed/~A/~A/" feed-slug user))))
    (when limit
      (push (cons "limit" (princ-to-string limit)) (quri:uri-query-params uri)))
    (when offset
      (push (cons "offset" (princ-to-string offset)) (quri:uri-query-params uri)))
    (cdr (assoc :results (api-get uri feed-slug user)))))

(defun create-activity (feed-slug user activity)
  (let ((uri (quri:uri (format nil "feed/~A/~A/" feed-slug user))))
    (api-post uri feed-slug user activity)))

(defun follow-feed (feed-slug user target)
  (let ((uri (quri:uri (format nil "feed/~A/~A/following/" feed-slug user))))
    (api-post uri feed-slug user (list (cons "target" target)))))

(defun feed-following (feed-slug user)
  (let ((uri (quri:uri (format nil "feed/~A/~A/following/" feed-slug user))))
    (api-get uri feed-slug user)))

(defun feed-followers (feed-slug user)
  (let ((uri (quri:uri (format nil "feed/~A/~A/followers/" feed-slug user))))
    (api-get uri feed-slug user)))

;; (defclass activity ()
;;   ((actor :initarg :actor
;; 		  :initform (error "Provide the actor")
;; 		  :accessor activity-actor)
;;    (verb :initarg :verb
;; 		 :initform (error "Provide the verb")
;; 		 :accessor activity-verb)
;;    (object :initarg :object
;; 		   :initform (error "Provide the object")
;; 		   :accessor activity-object)))

;; (defun serialize-activity (activity)
;;   (with-output-to-string (json:*json-output*)
;; 	(json:with-object ()
;; 	  (json:encode-object-member "actor" (actor activity)))))
